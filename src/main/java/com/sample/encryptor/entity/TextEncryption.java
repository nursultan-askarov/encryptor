package com.sample.encryptor.entity;

import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "text_encryption")
@Validated
public class TextEncryption implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "text_encryption_seq")
    @SequenceGenerator(name = "text_encryption_seq", sequenceName = "text_encryption_id_seq",
        allocationSize = 1)
    private Long id;

    private String text;

    private String encryptedText;
}
