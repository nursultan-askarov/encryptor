package com.sample.encryptor.repositories;

import com.sample.encryptor.entity.UserData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDataRepository extends JpaRepository<UserData, Long> {

    UserData findByUsernameAndRole(String username, String role);
}
