package com.sample.encryptor.repositories;

import com.sample.encryptor.entity.TextEncryption;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TextEncryptionRepository extends JpaRepository<TextEncryption, Long> {

    List<TextEncryption> findByText(String text);

    List<TextEncryption> findByEncryptedText(String encryptedText);
}
