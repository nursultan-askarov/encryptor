package com.sample.encryptor.api;

import com.sample.encryptor.commons.dto.UserDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Validated
public interface UserService {
    UserDTO createUserCredentials(@NotBlank String username, @NotBlank String password,
                                  @NotBlank String role) throws EncryptionException;
}
