package com.sample.encryptor.encryption;

import com.sample.encryptor.commons.exceptions.EncryptionErrorCodes;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static com.sample.encryptor.utils.EncryptionConstants.CIPHER_DECRYPT_MODE;
import static com.sample.encryptor.utils.EncryptionConstants.CIPHER_ENCRYPT_MODE;

@Slf4j
public abstract class EncryptionAlgorithm {

    public static PrivateKey getPrivateKey(String base64PrivateKey,
                                           String typeOfAlgorithm) throws EncryptionException {
        PrivateKey privateKey;
        KeyFactory keyFactory;
        try {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
                Base64.getDecoder().decode(base64PrivateKey.getBytes()));
            keyFactory = KeyFactory.getInstance(typeOfAlgorithm);
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new EncryptionException("Private key generation", EncryptionErrorCodes
                .PRIVATE_KEY_GENERATION_FAILURE, e);
        }
        return privateKey;
    }

    public static PublicKey getPublicKey(String base64PublicKey,
                                         String typeOfAlgorithm) throws EncryptionException {
        PublicKey publicKey;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(
                Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance(typeOfAlgorithm);
            publicKey = keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new EncryptionException("Public key generation", EncryptionErrorCodes
                .PUBLIC_KEY_GENERATION_FAILURE, e);
        }
        return publicKey;
    }

    public byte[] encryptData(String data, String publicKey, String typeOfAlgorithm,
                              String cipherInstance) throws EncryptionException {
        try {
            Cipher cipher = Cipher.getInstance(cipherInstance);
            cipher.init(CIPHER_ENCRYPT_MODE, getPublicKey(publicKey, typeOfAlgorithm));
            return cipher.doFinal(data.getBytes());
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException
            | NoSuchPaddingException | NoSuchAlgorithmException e) {
            throw new EncryptionException("Encryption failure", EncryptionErrorCodes
                .ENCRYPTION_DATA_FAILURE, e);
        }

    }

    public String decrypt(byte[] data, PrivateKey privateKey,
                          String cipherInstance) throws EncryptionException {
        try {
            Cipher cipher = Cipher.getInstance(cipherInstance);
            cipher.init(CIPHER_DECRYPT_MODE, privateKey);
            return new String(cipher.doFinal(data));
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException
            | NoSuchPaddingException | NoSuchAlgorithmException e) {
            throw new EncryptionException("Decryption failure", EncryptionErrorCodes
                .DECRYPTION_DATA_FAILURE);
        }

    }

    public String decryptData(String data, String base64PrivateKey, String typeOfAlgorithm,
                              String cipherInstance) throws EncryptionException {
        return decrypt(Base64.getDecoder().decode(data.getBytes()),
            getPrivateKey(base64PrivateKey, typeOfAlgorithm), cipherInstance);
    }

    public abstract String getTypeOfAlgorithm();

    public abstract String getCipherInstance();

}
