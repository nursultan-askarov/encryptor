package com.sample.encryptor.services;

import com.sample.encryptor.api.DecryptionService;
import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import com.sample.encryptor.commons.mappers.EncryptionMapper;
import com.sample.encryptor.encryption.RSAEncryption;
import com.sample.encryptor.entity.TextEncryption;
import com.sample.encryptor.repositories.TextEncryptionRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.sample.encryptor.utils.EncryptionConstants.PRIVATE_KEY;

@Service
@Slf4j
public class DecryptionImpl implements DecryptionService {

    @Autowired
    private EncryptionMapper mapper;

    @Autowired
    private RSAEncryption rsaEncryption;

    private final TextEncryptionRepository textEncryptionRepository;

    @Autowired
    public DecryptionImpl(TextEncryptionRepository textEncryptionRepository) {
        this.textEncryptionRepository = textEncryptionRepository;
    }

    @Override
    public EncryptionDTO decryptValue(String encryptedValue) throws EncryptionException {
        TextEncryption textEncryption = new TextEncryption();
        String initialText;
        TextEncryption encryptedData = retrieveData(encryptedValue);

        if (ObjectUtils.allNotNull(encryptedData)) {
            textEncryption = encryptedData;
            log.info("The text decryption has already been saved in the database");
        }

        if (StringUtils.isBlank(textEncryption.getText())) {
            String rsaAlgorithm = rsaEncryption.getTypeOfAlgorithm();
            String rsaCipher = rsaEncryption.getCipherInstance();

            initialText = rsaEncryption.decryptData(
                encryptedValue, PRIVATE_KEY, rsaAlgorithm, rsaCipher);
            log.info("The encrypted text {} was successfully decrypted ", encryptedValue);

            textEncryption = saveData(initialText, encryptedValue);
        }

        return mapper.map(textEncryption);
    }

    private TextEncryption retrieveData(String encryptedText) {
        return textEncryptionRepository.findByEncryptedText(encryptedText)
            .stream()
            .filter(n -> StringUtils.isNotBlank(n.getText()))
            .findFirst()
            .orElse(null);
    }

    private TextEncryption saveData(String text, String encryptedText) {
        TextEncryption data = TextEncryption
            .builder()
            .text(text)
            .encryptedText(encryptedText)
            .build();
        return textEncryptionRepository.save(data);
    }
}
