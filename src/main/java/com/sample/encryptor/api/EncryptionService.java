package com.sample.encryptor.api;

import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Validated
public interface EncryptionService {
    EncryptionDTO encryptValue(@NotBlank String text) throws EncryptionException;
}
