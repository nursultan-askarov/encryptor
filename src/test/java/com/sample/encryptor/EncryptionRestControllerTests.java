package com.sample.encryptor;

import com.sample.encryptor.api.EncryptionService;
import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.controllers.EncryptionRestController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(EncryptionRestController.class)
public class EncryptionRestControllerTests {

	private String text;
	private String encryptedText;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private EncryptionService encryptionService;

	EncryptionDTO encryptionDTO;

	@Before
	public void setUp() {
		text = "Kcell123";
		encryptedText = "ApGjq3TI7jLIwVi1dtGFnxP32YrXVCNmBFikcON0f9kqoPMhqV/anZfYtEToW8GpGxaHo" +
			"IgoB16SLpxMSi27I6XvFBbL/u7uuBLv1nOg3rQ0TKfJajcXIKkEVajg4hBohds3t5uu2uiP99nxWZ4eHQ" +
			"/cEQ4h3zP/+PlE9WK+1ss=";
	}

	@Test
	@WithMockUser(username = "user", password = "userPass", roles = "USER")
	public void encryptDataTest() throws Exception {
		encryptionDTO = EncryptionDTO.builder()
			.text(text)
			.encryptedValue(encryptedText)
			.build();

		given(encryptionService.encryptValue(text)).willReturn(encryptionDTO);

		mockMvc.perform(post("/encryptor/encrypt")
			.contentType(MediaType.TEXT_PLAIN)
			.param("text", text))
			.andDo(print())
			.andExpect(content().string("{\"text\":\"Kcell123\"," +
				"\"encryptedValue\":\"ApGjq3TI7jLIwVi1dtGFnxP32YrXVCNmBFikcON0f9kqoPMhqV/anZfYt" +
				"EToW8GpGxaHoIgoB16SLpxMSi27I6XvFBbL/u7uuBLv1nOg3rQ0TKfJajcXIKkEVajg4hBohds3t5u" +
				"u2uiP99nxWZ4eHQ/cEQ4h3zP/+PlE9WK+1ss=\"}"));
	}

}
