package com.sample.encryptor.controllers;

import com.sample.encryptor.api.EncryptionService;
import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping (
    path = "/encryptor"
)
@Slf4j
public class EncryptionRestController {

    private final EncryptionService encryptionService;

    @Autowired
    EncryptionRestController (EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @ResponseBody
    @PostMapping("/encrypt")
    public EncryptionDTO encryptValue(
        @RequestParam(value = "text") String text) throws EncryptionException {
        return encryptionService.encryptValue(text);
    }
}
