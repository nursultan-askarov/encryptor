package com.sample.encryptor.services;

import com.sample.encryptor.api.EncryptionService;
import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import com.sample.encryptor.commons.mappers.EncryptionMapper;
import com.sample.encryptor.encryption.RSAEncryption;
import com.sample.encryptor.entity.TextEncryption;
import com.sample.encryptor.repositories.TextEncryptionRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;

import static com.sample.encryptor.utils.EncryptionConstants.PUBLIC_KEY;

@Service
@Slf4j
public class EncryptionImpl implements EncryptionService {

    @Autowired
    private EncryptionMapper mapper;

    @Autowired
    private RSAEncryption rsaEncryption;

    private final TextEncryptionRepository textEncryptionRepository;

    @Autowired
    public EncryptionImpl(TextEncryptionRepository textEncryptionRepository) {
        this.textEncryptionRepository = textEncryptionRepository;
    }

    @Override
    public EncryptionDTO encryptValue(String text) throws EncryptionException {
        TextEncryption textEncryption = new TextEncryption();
        String encryptedText;
        TextEncryption encryptedData = retrieveData(text);

        if (ObjectUtils.allNotNull(encryptedData)) {
            textEncryption = encryptedData;
            log.info("Encrypted value for text {} has already been saved in the database", text);
        }

        if (StringUtils.isBlank(textEncryption.getEncryptedText())) {
            String rsaAlgorithm = rsaEncryption.getTypeOfAlgorithm();
            String rsaCipher = rsaEncryption.getCipherInstance();

            encryptedText = Base64.getEncoder().encodeToString(
                rsaEncryption.encryptData(text, PUBLIC_KEY, rsaAlgorithm, rsaCipher));
            log.info("The text {} was successfully encrypted ", text);

            textEncryption = saveData(text, encryptedText);
        }

        return mapper.map(textEncryption);
    }

    private TextEncryption retrieveData(String text) {
        return textEncryptionRepository.findByText(text)
            .stream()
            .filter(n -> StringUtils.isNotBlank(n.getEncryptedText()))
            .findFirst()
            .orElse(null);
    }

    private TextEncryption saveData(String text, String encryptedText) {
        TextEncryption data = TextEncryption
            .builder()
            .text(text)
            .encryptedText(encryptedText)
            .build();
        return textEncryptionRepository.save(data);
    }
}
