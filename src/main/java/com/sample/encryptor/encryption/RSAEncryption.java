package com.sample.encryptor.encryption;

import com.sample.encryptor.utils.EncryptionConstants;
import org.springframework.stereotype.Component;

@Component
public class RSAEncryption extends EncryptionAlgorithm {

    @Override
    public String getTypeOfAlgorithm() {
        return EncryptionConstants.RSA_ALGORITHM;
    }

    @Override
    public String getCipherInstance() {
        return EncryptionConstants.RSA_CIPHER_INSTANCE;
    }

}
