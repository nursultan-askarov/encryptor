package com.sample.encryptor.services;

import com.sample.encryptor.api.UserService;
import com.sample.encryptor.commons.dto.UserDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import com.sample.encryptor.commons.mappers.EncryptionMapper;
import com.sample.encryptor.entity.UserData;
import com.sample.encryptor.repositories.UserDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserImpl implements UserService {

    @Autowired
    private UserDataRepository userDataRepository;

    @Autowired
    private EncryptionMapper mapper;

    UserImpl(UserDataRepository userDataRepository) {
        this.userDataRepository = userDataRepository;
    }

    @Override
    public UserDTO createUserCredentials(String username, String password,
                                         String role) throws EncryptionException {
        UserData userCredentials = new UserData();
        UserData userData = retrieveData(username, role);

        if (ObjectUtils.allNotNull(userData)) {
            userCredentials = userData;
            log.info("User credentials for username {} has already been created in the database",
                username);
        }

        if (StringUtils.isBlank(userCredentials.getPassword())
            && StringUtils.isBlank(userCredentials.getRole())) {
            userCredentials = saveData(username, password, role);
            log.info("The user credentials for {} was successfully created ", username);
        }

        return mapper.map(userCredentials);
    }

    private UserData retrieveData(String username, String role) {
        return userDataRepository.findByUsernameAndRole(username, role);
    }

    private UserData saveData(String username, String password, String role) {
        UserData data = UserData
            .builder()
            .username(username)
            .password(password)
            .role(role)
            .build();
        return userDataRepository.save(data);
    }
}
