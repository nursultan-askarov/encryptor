package com.sample.encryptor.commons.exceptions;

import java.io.Serializable;

public enum EncryptionErrorCodes implements Serializable {
    PUBLIC_KEY_GENERATION_FAILURE,
    PRIVATE_KEY_GENERATION_FAILURE,
    ENCRYPTION_DATA_FAILURE,
    DECRYPTION_DATA_FAILURE;

    private String code;

    EncryptionErrorCodes() {
        this.code = this.name();
    }

    EncryptionErrorCodes(String code) {
        this.code = code;
    }

    public String getErrorCode() {
        return code;
    }

    public static String getId(EncryptionErrorCodes errorCode) {
        if (errorCode == null)
            return null;
        return errorCode.toString();
    }
}
