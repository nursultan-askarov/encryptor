package com.sample.encryptor.controllers;

import com.sample.encryptor.api.UserService;
import com.sample.encryptor.commons.dto.UserDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(
    path = "/user"
)
@Slf4j
public class UserRestController {

    private final UserService userService;

    @Autowired
    UserRestController(UserService userService) {
        this.userService = userService;
    }

    @ResponseBody
    @PostMapping("/create")
    public UserDTO encryptValue(
        @RequestParam(value = "username") String username,
        @RequestParam(value = "password") String password,
        @RequestParam(value = "role") String role) throws EncryptionException {
        return userService.createUserCredentials(username, password, role);
    }
}
