package com.sample.encryptor.controllers;

import com.sample.encryptor.api.DecryptionService;
import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.commons.exceptions.EncryptionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(
    path = "/encryptor"
)
@Slf4j
public class DecryptionRestController {

    private final DecryptionService decryptionService;

    @Autowired
    DecryptionRestController (DecryptionService decryptionService) {
        this.decryptionService = decryptionService;
    }

    @ResponseBody
    @PostMapping("/decrypt")
    public EncryptionDTO decryptValue(
        @RequestParam(value = "encryptedText") String encryptedValue) throws EncryptionException {
        return decryptionService.decryptValue(encryptedValue);
    }
}
