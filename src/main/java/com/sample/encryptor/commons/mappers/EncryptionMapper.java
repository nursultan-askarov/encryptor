package com.sample.encryptor.commons.mappers;

import com.sample.encryptor.commons.dto.EncryptionDTO;
import com.sample.encryptor.commons.dto.UserDTO;
import com.sample.encryptor.entity.TextEncryption;
import com.sample.encryptor.entity.UserData;
import org.springframework.stereotype.Component;

@Component
public class EncryptionMapper {

    public EncryptionDTO map(TextEncryption textEncryption) {
        return EncryptionDTO.builder()
            .text(textEncryption.getText())
            .encryptedValue(textEncryption.getEncryptedText())
            .build();
    }

    public UserDTO map(UserData userData) {
        return UserDTO.builder()
            .username(userData.getUsername())
            .password(userData.getPassword())
            .role(userData.getRole())
            .build();
    }
}
