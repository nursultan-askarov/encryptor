package com.sample.encryptor.commons.exceptions;

public class EncryptionException extends Exception {

    private EncryptionErrorCodes errorCode;

    public EncryptionException() {
        super();
    }

    public EncryptionException(String message) {
        super(message);
    }

    public EncryptionException(EncryptionErrorCodes errCode) {
        super();
        errorCode = errCode;
    }

    public EncryptionException(Throwable cause) {
        super(cause);
    }

    public EncryptionException(String message, EncryptionErrorCodes errCode) {
        super(message);
        errorCode = errCode;
    }

    public EncryptionException(EncryptionErrorCodes errCode, Throwable cause) {
        super(cause);
        errorCode = errCode;
    }

    public EncryptionException(String message, EncryptionErrorCodes errCode, Throwable cause) {
        super(message, cause);
        errorCode = errCode;
    }

    public String getErrorCode() {
        return EncryptionErrorCodes.getId(errorCode);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
